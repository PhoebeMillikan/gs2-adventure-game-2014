using UnityEngine;
using System.Collections;

public class MainMenuScript : MonoBehaviour
{
	public string startSceneName;
	
	Vector2 titlePos;
	Vector2 titleSize;
	Rect startArea;
	Rect versionArea;
	
	public GUIStyle titleLabelStyle = new GUIStyle();
	public GUIStyle startLabelStyle = new GUIStyle();
	
	float startAlpha = 0f;
	bool fadeOn;
	
	
	void Start()
	{
		int titleWidth = 500;
		int startWidth = 200;
		int startHeight = 50;
		int versionWidth = 30;
		int versionHeight = 20;
		
		startArea = new Rect(
			(Screen.width / 2) - (startWidth / 2),
			(Screen.height / 2) - (startHeight / 2),
			startWidth, startHeight);
		
		titleSize = new Vector2(titleWidth, 60);
		
		iTween.ValueTo(gameObject,
			iTween.Hash(
				"from",new Vector2(-titleWidth, 20),
				"to",new Vector2(20, 20), 
				"time",1f,
				"easeType",iTween.EaseType.spring,
				"onUpdate","UpdateTitlePos"));
		
		versionArea = new Rect(
			Screen.width - versionWidth - 5,
			Screen.height - versionHeight - 5,
			versionWidth, versionHeight);
	}
	
	void Update()
	{
		if (Input.GetMouseButtonDown(0))
			Application.LoadLevel(startSceneName);

		Color tempColor = startLabelStyle.normal.textColor;
		
		startAlpha = iTween.FloatUpdate(startAlpha, fadeOn ? 1f : 0f, 5f);
				
		if (startAlpha >= 0.9f)
			fadeOn = false;
		else if (startAlpha <= 0.1f)
			fadeOn = true;
		
		startLabelStyle.normal.textColor = 
			new Color(tempColor.r, tempColor.g, tempColor.b, startAlpha);
	}
	
	void UpdateTitlePos(Vector2 val)
	{
		titlePos = val;
	}
	
	void OnGUI()
	{
		if (Event.current.type != EventType.Repaint)
			return;
		
		GUI.Label(new Rect(titlePos.x, titlePos.y, 
			titleSize.x, titleSize.y),
			"Games Studio Adventure", titleLabelStyle);
		
		GUI.Label(startArea, "Click anywhere to start", startLabelStyle);
		
		GUI.Label(versionArea, "v1.0");
	}
}
