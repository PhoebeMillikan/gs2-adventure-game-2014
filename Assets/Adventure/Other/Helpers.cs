using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


/// <summary>
/// A list of shortcuts for making commmon changes in the game
/// or for getting common game variables / information.
/// </summary>
/// <remarks>
/// It's a great idea to make sure each helper function has a
/// short/clear summary documentation comment because these helpers
/// will be used a lot by multiple people.
/// </remarks>
public static class Helpers
{
	#region Player teleporting
	
	
	/// <summary>
	/// Instantly teleports player to target object in current scene,
	/// returns whether object is in current scene and teleport actually happened.
	/// </summary>
	public static bool TeleportPlayerInScene(string targetName)
	{
		GameObject obj = GameObject.Find(targetName);
		
		// not in current scene
		if (obj == null)
			return false;
		
		TeleportPlayerInScene(obj.transform);
		return true;
	}
	
	/// <summary>
	/// Instantly teleports player to target transform in current scene.
	/// </summary>
	public static void TeleportPlayerInScene(Transform transform)
	{
		if (transform == null)
			return;
		
		Player.Instance.Teleport(transform);
	}
	
	/// <summary>
	/// Instantly teleports player to target object in another scene.
	/// </summary>
	public static void TeleportPlayerInOtherScene(string targetName, string sceneName)
	{
		Application.LoadLevel(sceneName);
		Player.Instance.TeleportOnLevelLoad(targetName);
	}
	
	
	#endregion
	
	
	#region Player walking
	
	
	/// <summary>
	/// Makes player walk as close as possible to target object in current scene,
	/// returns whether object is in current scene and any walking actually happened.
	/// </summary>
	public static bool WalkPlayer(string targetName)
	{
		GameObject obj = GameObject.Find(targetName);
		
		// not in current scene
		if (obj == null)
			return false;
		
		Player.Instance.Movement.Move(obj.transform.position);
		return true;
	}
	
	/// <summary>
	/// Makes player walk as close as possible to a point in current scene.
	/// </summary>
	public static void WalkPlayer(Vector3 point)
	{
		Player.Instance.Movement.Move(point);
	}
	
	
	#endregion
	
	
	#region Object teleporting
	
	
	/// <summary>
	/// Instantly teleports object to target object in current scene,
	/// returns whether object is in current scene and teleport actually happened.
	/// </summary>
	public static bool Teleport(Interactable interactable, string targetName)
	{
		if (interactable == null)
			return false;
		
		GameObject go = GameObject.Find(targetName);
		
		// not in current scene
		if (go == null)
			return false;
		
		Teleport(interactable, go);
		return true;
	}
	
	/// <summary>
	/// Instantly teleports object to target transform in current scene.
	/// </summary>
	public static void Teleport(Interactable interactable, GameObject go)
	{
		if (interactable == null)
			return;
		
		if (go == null)
			return;
		
		interactable.Teleport(go);
	}
	
	
	#endregion
	
	
	#region Object getting
	
	
	/// <summary>
	/// Gets main game script / object.
	/// </summary>
	public static Game Game
	{
		get { return Game.Instance; }
	}
	
	/// <summary>
	/// Gets game's dialogue manager.
	/// </summary>
	public static DialogueManager DialogueManager
	{
		get { return Game.Instance.DialogueManager; }
	}
	
	/// <summary>
	/// Gets main player script / object.
	/// </summary>
	public static Player Player
	{
		get { return Player.Instance; }
	}
	
	/// <summary>
	/// Gets player's inventory.
	/// </summary>
	public static Inventory Inventory
	{
		get { return Player.Instance.Inventory; }
	}
	
	/// <summary>
	/// Gets main nav mesh script / object in current scene.
	/// </summary>
	public static AstarPath NavMesh
	{
		get
		{
			return (AstarPath)GameObject.FindObjectOfType(
				typeof(AstarPath)); 
		}
	}
	
	/// <summary>
	/// Gets the first transform in current scene with a particular name.
	/// </summary>
	public static Transform GetTransform(string name)
	{
		return Helpers.GetObject<Transform>(name);
	}
	
	/// <summary>
	/// Gets all transforms in current scene with a particular name.
	/// </summary>
	public static Transform[] GetAllTransforms(string name)
	{
		return Helpers.GetAllObjects<Transform>(name);
	}
	
	/// <summary>
	/// Gets the first game object in current scene with a particular name.
	/// </summary>
	public static GameObject GetGameObject(string name)
	{
		return Helpers.GetObject<GameObject>(name);
	}
	
	/// <summary>
	/// Gets all game objects in current scene with a particular name.
	/// </summary>
	public static GameObject[] GetAllGameObjects(string name)
	{
		return Helpers.GetAllObjects<GameObject>(name);
	}
	
	/// <summary>
	/// Gets the first interactable in current scene with a particular name.
	/// </summary>
	public static Interactable GetInteractable(string name)
	{
		return Helpers.GetObject<Interactable>(name);
	}
	
	/// <summary>
	/// Gets all interactables in current scene with a particular name.
	/// </summary>
	public static Interactable[] GetAllInteractables(string name)
	{
		return Helpers.GetAllObjects<Interactable>(name);
	}
	
	/// <summary>
	/// Gets the first object in current scene with a particular name
	/// and of any type.
	/// </summary>
	public static T GetObject<T>(string name)
		where T : UnityEngine.Object
	{
		T[] objects = (T[])GameObject.FindObjectsOfType(typeof(T));
		
		return Array.Find<T>(objects, o => (o.name == name));
	}
	
	/// <summary>
	/// Gets all objects in current scene with a particular name
	/// and of any type.
	/// </summary>
	public static T[] GetAllObjects<T>(string name)
		where T : UnityEngine.Object
	{
		T[] objects = (T[])GameObject.FindObjectsOfType(typeof(T));
		
		return Array.FindAll<T>(objects, o => (o.name == name));
	}
	
	
	#endregion
	
	
	#region Object layers
	
	
	/// <summary>
	/// Changes the layer of an object, can change layer of all child objects too.
	/// </summary>
	public static void ChangeLayer(GameObject go, int newLayer, bool changeChildren = false)
	{
		go.layer = newLayer;
		
		if (changeChildren)
		{
			foreach (Transform child in go.transform)
				child.gameObject.layer = newLayer;
		}
	}
	
	
	#endregion
	
	
	#region Navmesh
	
	
	/// <summary>
	/// Updates the first found nav mesh in current scene.
	/// </summary>
	public static void UpdateNavMesh()
	{
		AstarPath navMesh = NavMesh;
		
		if (navMesh != null)
			navMesh.Scan();
	}
	
	
	#endregion
	
	
	#region Instantiating objects
	
	
	/// <summary>
	/// Instantiates an interactable and returns the instance.
	/// </summary>
	public static Interactable InstantiateInteractable(Interactable i)
	{
		GameObject original = i.gameObject;
		GameObject instance = (GameObject)GameObject.Instantiate(original);
		
		instance.name = original.name;
		
		return (Interactable)instance.GetComponent(typeof(Interactable));
	}
	
	
	#endregion
	
	
	#region Active / inactive objects
	
	
	/// <summary>
	/// Sets a game object to active, also applies to its child objects.
	/// </summary>
	public static void SetGameObjectActive(GameObject go)
	{
		go.active = true;
		
		Transform[] transforms = go.GetComponentsInChildren<Transform>();
		foreach (Transform t in transforms)
			t.gameObject.active = true;
	}
	
	/// <summary>
	/// Sets a game object to inactive, also applies to its child objects.
	/// </summary>
	public static void SetGameObjectInactive(GameObject go)
	{
		go.active = false;
		
		Transform[] transforms = go.GetComponentsInChildren<Transform>();
		foreach (Transform t in transforms)
			t.gameObject.active = false;
	}
	
	
	#endregion
}
